# Gravitas

# Overview
Gravitas is a web concultant for business from England.

- Project Type: Freelance
- Responsive: Yes (using tailwind)

# Tech Stack
- HTML
- SCSS
- Tailwind

# Preview
![mobile](screenshot/01.PNG)
![mobile](screenshot/02.PNG)
